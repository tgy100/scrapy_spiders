# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Top250Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # 图片
    image = scrapy.Field()
    # 电影图片存放位置
    local_path = scrapy.Field()
    # 电影名
    name = scrapy.Field()
    #电影简介
    description = scrapy.Field()
    # 评分
    start = scrapy.Field()
    #评价
    comment = scrapy.Field()
