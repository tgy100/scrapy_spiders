# coding: utf-8
import scrapy
from top250.items import Top250Item


class TopSpider(scrapy.Spider):
    name = 'top250'
    allow_domains = ['movie.douban.com']
    start = 0
    url = 'https://movie.douban.com/top250?start='
    start_urls = [ url + str(start),]

    def parse(self, response):
        pass
        bases = response.xpath('//div[@class="item"]')
        for bs in bases:
            item = Top250Item()
            item['image'] = bs.xpath('.//div[@class="pic"]//img/@src').extract()[0]
            item['name'] = bs.xpath('.//div[@class="info"]//a/span[1]/text()').extract()[0]
            item['description'] =''.join(bs.xpath('.//div[@class="info"]/div[@class="bd"]/p/text()').extract()).replace(' ','').replace('\n','')
            item['start'] = bs.xpath('.//div[@class="info"]//div[@class="star"]/span[@class="rating_num"]/text()').extract()[0]
            item['comment'] = bs.xpath('.//div[@class="info"]//span[@class="inq"]/text()').extract()[0]
            # print(image, name, description, start, comment)
            yield item

        if self.start < 225:
            self.start += 25
            yield scrapy.Request(url= self.url + str(self.start), callback=self.parse)

