# coding: utf-8
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request, FormRequest, HtmlResponse

class GHSpider(CrawlSpider):
    name = 'github'
    allow_domains = ['github.com']
    start_urls = ['']
    post_headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh-CN,zh;q=0.8,en;q=0.6",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36",
        "Referer": "https://github.com/",
    }

    def start_requests(self):
        return [scrapy.Request(url='https://github.com/login', meta={'cookiejar':1},
                               callback= self.parse_login_page),]


    def parse_login_page(self, response):
        authenticity_token = response.xpath('//form//input[@name="authenticity_token"]/@value').extract()[0]
        # print(authenticity_token, response.meta['cookiejar'])
        cookies = response.headers.getlist('Set-Cookie')
        # print(cookies[0], cookies[1])
        # print('------', ';'.join(cookies).decode('utf-8') ,'-----------')
        return FormRequest.from_response(response,
                                         url='https://github.com/session',
                                         meta={'cookiejar': response.meta['cookiejar']},
                                         headers = self.post_headers,
                                         formdata={
                                             'commit':'Sign in',
                                             'utf8': '✓',
                                             'authenticity_token': authenticity_token,
                                              'login': '799053179@qq.com',
                                              'password': 'tgy19922956'
                                         },
                                         callback = self.pase_login_success,
                                         dont_click=True)

    def pase_login_success(self, response):
        pass
        print(response.body)