# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json


class ZLPipeline(object):

    def __init__(self):
        super(ZLPipeline, self).__init__()
        self.sava_file = open('zhilian.json','wb')

    def process_item(self, item, spider):

        json_data = json.dumps(dict(item), ensure_ascii= False) +'\n'
        self.sava_file.write(json_data.encode('utf-8'))
        return item

    def close_spider(self, spider):
        self.sava_file.close()
