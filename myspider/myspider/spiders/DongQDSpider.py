# coding: utf-8
import scrapy
import json
from myspider.items import *

class DQDSpider(scrapy.Spider):
    name = 'dongqd'
    allowed_domains = ['api.dongqiudi.com']
    start_urls = ['https://api.dongqiudi.com/app/tabs/iphone/1.json?after=1512083102&page=2&mark=gif&version=576']

    def parse(self, response):
        pass
        response_data = json.loads(response.text)
        data = response_data['articles']
        next = response_data['next']
        print(next)

        for data_in in data:
            pass
            item = DongQDItem()
            item['id'] = data_in['id']
            item['title'] = data_in['title']
            item['thumb'] = data_in['thumb']
            yield item
