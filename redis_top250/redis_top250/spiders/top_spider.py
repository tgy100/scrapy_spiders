# coding: utf-8
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule, CrawlSpider
from scrapy_redis.spiders import RedisCrawlSpider


class TopSpider(CrawlSpider):
    pass
    name = 'top250'
    allowed_domains = ['movie.douban.com']
    start_urls = ['https://movie.douban.com/top250?start=0']
    # redis_key = 'topSpider:start_urls'

    rules = [
                Rule(link_extractor=LinkExtractor(allow='/top250\?start=\d+&.*'), callback='parse_top', follow=True),
            ]

    # def __init__(self, *args, **kwargs):
    #     domain = kwargs.pop('domain', '')
    #     self.allowed_domains = filter(None, domain.split(','))
    #     super(TopSpider, self).__init__()

    def parse_top(self, response):
        pass
        '''
            主  //div[@class="item"]
            index ./div[@class="pic"]/em/text()
            img_url ./div[@class="pic"]/a/img/@src
            name  ./div[@class="info"]/div[@class="hd"]/a/span[1]
            导演 主演 年份 类型 ./div[@class="info"]/div[@class="bd"]/p[1]/text()
            评分 ./div[@class="info"]/div[@class="bd"]/div[@class="star"]/span[@class="rating_num"]/text()
            评论人数  ./div[@class="info"]/div[@class="bd"]/div[@class="star"]/span[4]/text()
            简评 .//div[@class="info"]/div[@class="bd"]/p[@class="quote"]/span/text()
        '''
        print(response.url)
        base_xh = response.xpath('//div[@class="item"]')
        for bx in base_xh:
            index = bx.xpath('./div[@class="pic"]/em/text()').extract()[0]
            img_url = bx.xpath('./div[@class="pic"]/a/img/@src').extract()[0]
            name = bx.xpath('./div[@class="info"]/div[@class="hd"]/a/span[1]/text()').extract()[0]
            part_str = bx.xpath('./div[@class="info"]/div[@class="bd"]/p[1]/text()').extract()
            self.todo_partallStr(part_str)
            # print( part_str)

    def todo_partallStr(self, part_str):
        pass
        one = part_str[0].strip().replace('...', '')
        two = part_str[1].strip()
        print(one, two)

