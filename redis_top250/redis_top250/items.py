# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Top250Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
    index = scrapy.Field()
    name = scrapy.Field()
    image = scrapy.Field()
    director = scrapy.Field() #导演
    role = scrapy.Field() #主演
    year = scrapy.Field()
    country = scrapy.Field()
    type = scrapy.Field() #电影类型
    score = scrapy.Field() #评分
    score_man = scrapy.Field() #评论人数
    description = scrapy.Field() #评语





