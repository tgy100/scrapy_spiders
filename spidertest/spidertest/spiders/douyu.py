# coding: utf-8
import scrapy

class DouYuSpider(scrapy.Spider):
    name = 'douyu'
    allowed_domains = ['douyu.com']
    start_urls = [
        "https://www.douyu.com/directory/all",
    ]

    def parse(self, response):
        pass
        # print(response.body)
        base_xpath = response.xpath('//ul[@id="live-list-contentbox"]//a[@class="play-list-link"]')
        for base in base_xpath:
            title = base.xpath('./@title').extract()
            url = base.xpath('./span[@class="imgbox"]/img/@src').extract()
            num = base.xpath('./div[@class="mes"]//span[@class="dy-num fr"]/text()').extract()
            print(title[0], url[0], num[0])